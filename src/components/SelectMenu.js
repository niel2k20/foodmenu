import React, { Component } from 'react'

export default class SelectMenu extends Component {

    state = {
        checked: false
    }
    renderOption = () => {
        return (
            this.props.menu.map((items, index) => {
                return (
                    <option key={index} value={index}>{items.category}</option>
                );
            })
        );
    }

    inStocks = (e) => this.props.onSelect(e.target.value)
    isChecked = async () => {
        await this.setState({
            checked: !this.state.checked
        })
        this.props.selectCheckbox(this.state.checked)
    }
    render() {

        return (
            <div>
                <select onChange={(e) => this.inStocks(e)}>

                    {this.renderOption()}
                </select>

                <br />
                <input type="checkbox"
                    onChange={() => this.isChecked()} />   <label>Show Items only in Stock Items. If Selected</label>  

            </div>
        )
    }
}
