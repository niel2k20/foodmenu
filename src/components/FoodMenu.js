import React from 'react'
import SubItems from './SubItems';

export default function FoodMenu({ menu, selectedIndex ,isChecked }) { 
 
    function renderMenu() {
        if (isChecked ===false) {
            return (
                menu.map((items) => { 
                    return <SubItems key={items.category} menu={items} />
                })
            );
        }
    }
    function renderIfSelected() {

        if (selectedIndex !== null && isChecked !==false) {  
            return (
                [menu[selectedIndex]].map((items) => { 
                    return <SubItems key={items.category} menu={items}  selectedIndex={selectedIndex}/>
                })
            );
        } 
    }
    return (
        <div>

            <h3>Menu Items</h3>

            {renderMenu()}
            {renderIfSelected()}

        </div>
    );
}
