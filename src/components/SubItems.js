import React from 'react'

export default function SubItems({ menu, selectedIndex }) { 
    function renderItems() {

        if (selectedIndex !== undefined) {

            return (
                menu.items.map(filteredMenu => (
                    <div  key={filteredMenu.name}>
                        {filteredMenu.inStock !== false ? <div><p>{filteredMenu.name} ----------------{filteredMenu.price}</p></div> : null}
                    </div>

                ))
            )
        }
        else {
            return (
                menu.items.map((items) => {
                    return (
                        <p key={items.name}>{items.name} ----------------{items.price}</p>
                    )
                })
            )
        }
    }

    return (
        <div>
            <h2>{menu.category}</h2>
            {renderItems()}

        </div>
    )
}
